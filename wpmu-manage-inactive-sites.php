<?php
/**
 * Inactive Site Manager
 *
 * @package     WPMUDEV-ISM
 * @author      Shady Sharaf
 * @license     GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: Inactive Site Manager
 * Plugin URI:  https://bitbucket.org/shadyvb/wpmu-inactive-site-manager
 * Description: Configurable, Cron-based management of Inactive Sites in a Multisite WordPress Installations.
 * Version:     0.1
 * Author:      Shady Sharaf
 * Author URI:  https://sharaf.me
 * Text Domain: wpmudev-mis
 * Network:     true
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

namespace WPMUDEV\ISM;

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Cheatin\', ha!?' );
}

// Register autoloader.
spl_autoload_register(
	function( $full_class_name ) {
		// TODO: Add class map caching.
		$index = strpos( $full_class_name, __NAMESPACE__ );
		if ( 0 === $index ) {
			$class_name = str_replace( __NAMESPACE__ . '\\', '', $full_class_name );
			$refined = str_replace( '_', '-', strtolower( $class_name ) );
			require_once __DIR__ . '/php/class-' . $refined . '.php';
		}
	}
);

// Setup plugin filters.
add_action( 'init', array( __NAMESPACE__ . '\Plugin', 'setup' ) );
