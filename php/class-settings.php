<?php

namespace WPMUDEV\ISM;

/**
 * Class Admin_Settings
 *
 * @package WPMUDEV\ISM
 */
class Settings {

	/**
	 * Singleton instance
	 *
	 * @var \WPMUDEV\ISM\Settings
	 */
	public static $instance;

	/**
	 * Singleton helper
	 *
	 * @return \WPMUDEV\ISM\Settings
	 */
	public static function instance() {
		if ( empty( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Settings/Option key
	 */
	const PREFIX = 'wpmudev-ism';

	/**
	 * Plugin config, loaded from database
	 *
	 * @var array
	 */
	public $config = array();

	/**
	 * Class entry point, sets up all required actions and filters.
	 */
	public static function setup() {
		$instance = self::instance();

		$instance->load_config();

		if ( is_admin() ) {
			// Register settings page.
			add_action( 'network_admin_menu', array( $instance, 'register_settings' ) );

			// Register settings options.
			add_action( 'network_admin_menu', array( $instance, 'register_settings_page' ) );

			// Save settings.
			add_action( sprintf( 'network_admin_edit_%s', $instance->prefix( 'page' ) ),  array( $instance, 'save_settings' ) );
		}
	}

	/**
	 * Load config values from network options
	 */
	public function load_config() {
		$defaults = wp_list_pluck( $this->get_config_fields(), 'default' );
		$this->config = array_merge( $defaults, get_network_option( false, $this->prefix( 'config' ), array() ) );
	}

	/**
	 * Register settings via WordPress Settings API.
	 *
	 * @action admin_init
	 */
	public function register_settings() {
		if ( false === get_network_option( false, $this->prefix( 'config' ) ) ) {
			add_network_option( false, $this->prefix( 'config' ), $this->get_config_fields() );
		}

		add_settings_section(
			self::prefix( 'config' ),
			__( 'WPMUDEV - Inactive Site Manager', 'wpmudev-ism' ),
			array( $this, 'render_section_info' ),
			$this->prefix( 'config' )
		);

		foreach ( $this->get_config_fields() as $key => $field ) {
			add_settings_field(
				self::prefix( 'config-' . $key ),
				$field['title'],
				array( $this, 'render_settings_field' ),
				self::prefix( 'config' ),
				self::prefix( 'config' ),
				array( $key, $field )
			);

		}
	}

	/**
	 * Register Settings page/menu.
	 *
	 * @action network_admin_menu
	 */
	public function register_settings_page() {
		add_submenu_page(
			'settings.php',
			__( 'WPMUDEV - Inactive Sites Manager', 'wpmudev-ism' ),
			__( 'Inactive Sites', 'wpmudev-ism' ),
			'manage_network_options',
			$this->prefix( 'page' ),
			array( $this, 'render_settings_page' )
		);
	}

	/**
	 * Renders settings page content.
	 */
	public function render_settings_page() {
		if ( isset( $_GET['updated'] ) ) { // WPCS: input var okay.
			printf( '<div id="message" class="updated notice is-dismissible"><p>%s</p></div>', esc_attr__( 'Options saved', 'wpmudev-ism' ) );
		}

		$url = add_query_arg( 'action', $this->prefix( 'page' ), 'edit.php' );
		?>
		<div class="wrap">
			<form method="POST" action="<?php echo esc_url( $url ); ?>">
				<?php
					settings_fields( $this->prefix( 'page' ) );
					do_settings_sections( $this->prefix( 'config' ) );
					submit_button( __( 'Save changes', 'wpmudev-ism' ), 'primary', 'submit', false );
					echo ' ';
					submit_button( __( 'Save and Execute Now', 'wpmudev-ism' ), 'primary', 'execute', false );
				?>
			</form>
		</div>
		<?php
	}

	/**
	 * Render informations about current settings/status
	 */
	public function render_section_info() {
		echo '<strong>';

		$blogs = Manager::instance()->get_expired_sites( -1, true );
		printf(
			_n(
				__( 'Currently expired site(s), according to saved configurations, is <i>%d site</i>.', 'wpmudev-ism' ),
				__( 'Currently expired site(s), according to saved configurations, is <i>%d sites</i>.', 'wpmudev-ism' ),
				$blogs['count']
			),
			$blogs['count']
		);

		echo '<br/>';

		$last_run = get_site_option( Settings::instance()->prefix( 'last_run' ) );
		if ( $last_run ) {
			printf(
				__( 'Last operation ran on %s.', 'wpmudev-ism' ),
				date( 'r', $last_run )
			);
			echo '<br/>';
		}

		$next_job = wp_next_scheduled( Cron::HOOK_NAME );
		if ( $next_job ) {
			printf(
				__( 'Next operation runs at %s.', 'wpmudev-ism' ),
				date( 'r', $next_job )
			);
		} else {
			_e( 'No operation is scheduled, check your settings if that is not the expected case.', 'wpmudev-ism' );
		}

		echo '<br/>';

		$success_count = Manager::instance()->get_purged_count();
		$error_count = Manager::instance()->get_errors_count();
		vprintf(
			__( 'Successfully handled %d sites, and failed to handle %d sites, since plugin installation.', 'wpmudev-ism' ),
			array(
				$success_count,
				$error_count,
			)
		);

		echo '</strong>';
	}

	/**
	 * Render a settings field, based on field definition by get_config_fields()
	 *
	 * @param array $args Field object.
	 */
	public function render_settings_field( $args ) {
		list( $key, $field ) = $args;

		switch ( $field['type'] ) {
			case 'checkbox':
				vprintf( '<input type="checkbox" name="%1$s" value="%2$s" %3$s />', array(
					esc_attr( $key ),
					1,
					checked( $this->config[ $key ], true, false ),
				) );
				break;
			case 'select':
				vprintf( '<select name="%1$s">', array(
					esc_attr( $key ),
				) );
				foreach ( $field['options'] as $option_value => $option_title ) {
					vprintf( '<option value="%1$s" %2$s>%3$s</option>', array(
						esc_attr( $option_value ),
						selected( $this->config[ $key ], $option_value, false ),
						esc_attr( $option_title ),
					) );
				}
				echo '</select>';
				break;
			default:
				vprintf( '<input type="%3$s" name="%1$s" value="%2$s" />', array(
					esc_attr( $key ),
					esc_attr( $this->config[ $key ] ),
					esc_attr( $field['type'] ),
				) );
				break;
		}

		if ( isset( $field['hint'] ) ) {
			printf( '<p><small>%s</small></p>', esc_html( $field['hint'] ) );
		}
	}

	/**
	 * Callback for saving the options form
	 */
	public function save_settings() {
		check_admin_referer( $this->prefix( 'page' ) . '-options' );

		$fields = $this->get_config_fields();
		$old_config = $this->config;
		$config = wp_list_pluck( $fields, 'default' );

		foreach ( $fields as $key => $field ) {
			if ( isset( $_POST[ $key ] ) ) { // WPCS: input var okay.
				$config[ $key ] = apply_filters( 'sanitize_option_' . $key, wp_unslash( $_POST[ $key ] ) ); // WPCS: input var okay, sanitization okay.
			}
		}
		update_network_option( false, $this->prefix( 'config' ), $config );

		$this->config = $config;

		// Update cron job if the frequency changed, or plugin is deactivated ( from Settings ).
		if ( empty( $config['activated'] ) ) {
			Cron::instance()->remove();
		} elseif ( $config['frequency'] !== $old_config['frequency'] ) {
			Cron::instance()->reset();
		}

		// Handling immediate execution.
		if ( isset( $_POST['execute'] ) ) { // WPCS: input var okay.
			Manager::instance()->check();
		}

		wp_redirect(
			add_query_arg(
				array(
					'page'    => $this->prefix( 'page' ),
					'updated' => 'true',
				),
				network_admin_url( 'settings.php' )
			)
		);

		exit;
	}

	/**
	 * Get config options
	 *
	 * @return array
	 */
	public function get_config_fields() {
		return array(
			'activated' => array(
				'title' => __( 'Activated', 'wpmudev-ism' ),
				'hint' => __( 'Activate or deactivate the plugin functionality.', 'wpmudev-ism' ),
				'default' => 0,
				'type' => 'checkbox',
			),
			'frequency' => array(
				'title' => __( 'Frequency', 'wpmudev-ism' ),
				'hint' => __( 'Frequency of the sweeping check.', 'wpmudev-ism' ),
				'default' => 'daily',
				'type' => 'select',
				'options' => array(
					'daily' => __( 'Daily', 'wpmudev-ism' ),
					'weekly' => __( 'Weekly', 'wpmudev-ism' ),
					'monthly' => __( 'Monthly', 'wpmudev-ism' ),
				),
			),
			'expiry' => array(
				'title' => __( 'Expiry', 'wpmudev-ism' ),
				'hint' => __( 'The amount of months after which the site is considered expired.', 'wpmudev-ism' ),
				'default' => 3,
				'type' => 'number',
			),
			'per_action' => array(
				'title' => __( 'Sites per action', 'wpmudev-ism' ),
				'hint' => __( 'Number of expired sites to act upon per a single day, leave empty to act on all sites at once.', 'wpmudev-ism' ),
				'default' => '',
				'type' => 'number',
			),
			'tod' => array(
				'title' => __( 'Time of day', 'wpmudev-ism' ),
				'hint' => __( 'Preferred time of day to execute the purge process.', 'wpmudev-ism' ),
				'default' => '00:00',
				'type' => 'time',
			),
			'action'    => array(
				'title' => __( 'Action', 'wpmudev-ism' ),
				'hint' => __( 'What happens to expiring sites ?.', 'wpmudev-ism' ),
				'default' => 'disable',
				'type' => 'select',
				'options' => array(
					'archive' => __( 'Archive', 'wpmudev-ism' ),
					'deactivate' => __( 'Deactivate', 'wpmudev-ism' ),
					'delete' => __( 'Hard Delete', 'wpmudev-ism' ),
				),
			),
		);
	}

	/**
	 * Return a prefixed option key name
	 *
	 * @param string $key String to prefix.
	 *
	 * @return string
	 */
	public function prefix( $key ) {
		return self::PREFIX . '-' . $key;
	}

	/**
	 * Quick helper to get a config value through key name
	 *
	 * @param string $key Config key.
	 *
	 * @return mixed
	 */
	public static function get( $key ) {
		return self::instance()->config[ $key ];
	}
}
