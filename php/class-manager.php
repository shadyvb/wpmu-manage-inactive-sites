<?php

namespace WPMUDEV\ISM;

/**
 * Class Manager
 *
 * @package WPMUDEV\ISM
 */
class Manager {

	/**
	 * Singleton instance
	 *
	 * @var \WPMUDEV\ISM\Manager
	 */
	public static $instance;

	/**
	 * Singleton helper
	 *
	 * @return \WPMUDEV\ISM\Manager
	 */
	public static function instance() {
		if ( empty( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Setup class actions and filters
	 */
	public static function setup() {
		$instance = self::instance();

		// Register the cron action.
		add_action( Cron::HOOK_NAME, array( $instance, 'check' ) );
	}

	/**
	 * Main plugin routine, checking for expired blogs and handling them
	 *
	 * @action wpmudev-ism-cron
	 */
	public function check() {
		// Get blog IDs.
		$expired = $this->get_expired_sites( Settings::get( 'per_action' ) );
		$blog_ids = $expired['blogs'];
		$total = $expired['count'];

		// Get blogs data.
		$blogs_data = array_combine( $blog_ids, array_map( 'get_blog_details', $blog_ids, array() ) );
		$results = array();

		// Perform specified action on all selected blogs.
		foreach ( $blog_ids as $blog_id ) {
			$result = $this->action( $blog_id );
			$results[ $blog_id ] = $blogs_data[ $blog_id ];
			$results[ $blog_id ]->result = $result;
		}

		if ( $total > count( $blog_ids ) ) {
			// Execute the routine again tomorrow to finish the remaining of the batch.
			Cron::instance()->schedule( true );
		}

		// Logging.
		$this->increment_purged( wp_list_filter( $results, array( 'result' => true ) ) );
		$this->increment_errors( wp_list_filter( $results, array( 'result' => false ) ) );
		$this->log_operation( $results );
		$this->email_notifications( $results );
	}

	/**
	 * Perform the specific action on a selected blog
	 *
	 * @param int $blog_id Blog ID to handle.
	 *
	 * @return bool
	 */
	public function action( $blog_id ) {
		global $wpdb;

		// TODO: Introduce another cron to actually do the action, to allow admins ( or subsite owners ) to revert,
		// or stop the action from being done.
		switch ( Settings::get( 'action' ) ) {
			case 'archive':
				update_blog_status( $blog_id, 'archived', 1 );
				break;
			case 'deactivate':
				wpmu_delete_blog( $blog_id, false );
				break;
			case 'delete':
				wpmu_delete_blog( $blog_id, true );
				break;
			default:
				return false;
		}

		return true;
	}

	/**
	 * Get expired sites according to saved settings
	 *
	 * @param int  $limit      Limit results.
	 * @param bool $count_only Get only total count of sites.
	 *
	 * @return false|int|array
	 */
	public function get_expired_sites( $limit = 10, $count_only = false ) {
		global $wpdb;
		$expiry_months = Settings::get( 'expiry' );
		$expiry_limit = time() - $expiry_months * MONTH_IN_SECONDS;
		$expiry_date = date( 'Y-m-d H:i:s', $expiry_limit );

		// Format the query.
		$query = $wpdb->prepare(
			sprintf(
				'SELECT SQL_CALC_FOUND_ROWS blog_id FROM %s WHERE last_updated < DATE( %%s ) AND blog_id > 1 AND archived < 1 AND deleted < 1',
				$wpdb->blogs
			),
			$expiry_date
		);

		if ( ! $count_only && $limit > 0 ) {
			$query = sprintf( '%s LIMIT %d', $query, intval( $limit ) );
		}

		if ( $count_only ) {
			$blogs = null;
			$wpdb->query( $query ); // WPCS: db call okay, cache okay.
			$count = $wpdb->get_var( 'SELECT FOUND_ROWS()' ); // WPCS: db call okay, cache okay.
		} else {
			$blogs = $wpdb->get_col( $query ); // WPCS: db call okay, cache okay.
			$count = $wpdb->get_var( 'SELECT FOUND_ROWS()' ); // WPCS: db call okay, cache okay.
		}

		return compact( 'blogs', 'count' );
	}

	/**
	 * Keep track of successfully purged blogs count
	 *
	 * @param array $blogs List of successfully handled blogs.
	 */
	public function increment_purged( $blogs ) {
		$key = Settings::instance()->prefix( 'purged' );
		$existing = get_site_option( $key, false );

		if ( false === $existing ) {
			add_site_option( $key, count( $blogs ) );
		} else {
			update_site_option( $key, count( $blogs ) + intval( $existing ) );
		}
	}

	/**
	 * Keep track of successfully purged blogs count
	 *
	 * @param array $blogs List of errors during handling blogs.
	 */
	public function increment_errors( $blogs ) {
		$key = Settings::instance()->prefix( 'errors' );
		$existing = get_site_option( $key, false );

		if ( false === $existing ) {
			add_site_option( $key, count( $blogs ) );
		} else {
			update_site_option( $key, count( $blogs ) + intval( $existing ) );
		}
	}

	/**
	 * Get count of successfully handled sites, since installation of the plugin
	 *
	 * @return int
	 */
	public function get_purged_count() {
		return get_site_option( Settings::instance()->prefix( 'purged' ), 0 );
	}

	/**
	 * Get count of errors during handling sites, since installation of the plugin
	 *
	 * @return int
	 */
	public function get_errors_count() {
		return get_site_option( Settings::instance()->prefix( 'errors' ), 0 );
	}

	/**
	 * Log each operation results, for further analysis / error debugging
	 *
	 * @param array $blogs Lists of blogs handled within a single operation.
	 */
	public function log_operation( $blogs ) {
		/*
		TODO: Fix this, since it'll store in whichever site is being used atm, not the master site, and I fear that
		a switch_to_blog statement might make this more expensive than it should
		*/

		/*
		$count = count( $blogs );
		$successful = count( array_filter( $blogs ) );

		$postarr = array(
			'post_type' => 'wpmudev-ism-log',
		    'post_content' => wp_json_encode( $blogs ),
			'post_title' => $successful === $count ? 'success' : 'error',
		);

		wp_insert_post( $postarr );
		*/

		update_site_option( Settings::instance()->prefix( 'last_run' ), time() );
	}

	/**
	 * Notify both Network Admin and SubSite Admin of the operation
	 *
	 * @param array $blogs Lists of blogs handled within a single operation.
	 */
	public function email_notifications( $blogs ) {

		if ( empty( $blogs ) ) {
			return;
		}

		// First, Notify Network Admin.
		$contenttype_callback = function(){
			return 'text/html';
		};
		$to = get_site_option( 'admin_email' );
		$subject = sprintf( __( 'Inactive Sites on %s', 'wpmudev-ism' ), get_site_option( 'site_name' ) );

		$blog_list = array_map(
			function( $blog ) {
				return sprintf( '<li><a href="%s">%s</a></li>', $blog->home, $blog->blogname );
			},
			$blogs
		);

		$message = vsprintf(
			__( '<p>Dear Admin,</p><p>The following blogs from %1$s have been purged because of inactivity.</p><ul>%2$s</ul>', 'wpmudev-ism' ),
			array(
				get_site_option( 'site_name' ),
				implode( ',', $blog_list ),
			)
		);

		add_filter( 'wp_mail_content_type', $contenttype_callback );
		wp_mail( $to, $subject, $message );
		remove_filter( 'wp_mail_content_type', $contenttype_callback );

		// Then, notify each sub-site/blog owner/administrator.
		foreach ( $blogs as $blog ) {
			$subject = __( 'Inactive Site Deactivation Notification', 'wpmudev-ism' );
			$message = vsprintf(
				__( '<p>Dear Site Owner,</p><p>Your site ( %1$s / %2$s ) has been deactivated/deleted because it exceeded the inactivity allowance by our network.</p>', 'wpmudev-ism' ),
				array(
					$blog->blogname,
					$blog->home,
				)
			);

			add_filter( 'wp_mail_content_type', $contenttype_callback );
			wp_mail( get_blog_option( $blog->blog_id, 'admin_email' ), $subject, $message );
			remove_filter( 'wp_mail_content_type', $contenttype_callback );
		}
	}
}
