<?php

namespace WPMUDEV\ISM;

/**
 * Class Plugin
 *
 * @package WPMUDEV\ISM
 */
class Plugin {

	/**
	 * Setup the plugin actions/filters and initialize other classes.
	 */
	public static function setup() {
		// Load config, and register settings/menus if in admin.
		Settings::setup();

		// Setup Cron job/schedule.
		Cron::setup();

		// Setup the scheduled checking process.
		Manager::setup();
	}
}
