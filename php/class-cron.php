<?php

namespace WPMUDEV\ISM;

/**
 * Class Cron
 *
 * @package WPMUDEV\ISM
 */
class Cron {

	/**
	 * Singleton instance
	 *
	 * @var \WPMUDEV\ISM\Cron
	 */
	public static $instance;

	/**
	 * Singleton helper
	 *
	 * @return \WPMUDEV\ISM\Cron
	 */
	public static function instance() {
		if ( empty( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Cron job frequency
	 *
	 * @var int
	 */
	const HOOK_NAME = 'wpmudev-ism-cron';

	/**
	 * Setup the actions and filters
	 */
	public static function setup() {
		$instance = self::instance();

		// Register our additional schedule intervals.
		add_filter( 'cron_schedules', array( $instance, 'register_additional_intervals' ) );

		// Schedule the cron job.
		$instance->schedule();
	}

	/**
	 * Add more schedule intervals, Weekly and Monthly
	 *
	 * @param array $schedules Existing Cron schedule intervals.
	 *
	 * @return mixed
	 */
	public function register_additional_intervals( $schedules ) {
		if ( empty( $schedules['weekly'] ) ) {
			$schedules['weekly']  = array( 'interval' => WEEK_IN_SECONDS, 'display' => __( 'Weekly', 'wpmudev-ism' ) );
		}
		if ( empty( $schedules['monthly'] ) ) {
			$schedules['monthly'] = array( 'interval' => MONTH_IN_SECONDS, 'display' => __( 'Monthly', 'wpmudev-ism' ) );

		}
		return $schedules;
	}

	/**
	 * Reset the cron job, upon changing the frequency
	 */
	public function reset() {
		$this->remove();
		$this->schedule();
	}

	/**
	 * Schedule the event
	 *
	 * @param bool $force Force scheduling of the next available time.
	 */
	public function schedule( $force = false ) {
		$time = strtotime( 'tomorrow ' . Settings::get( 'tod' ) );

		if ( $force || wp_next_scheduled( self::HOOK_NAME ) < time() ) {
			$this->remove();
			wp_schedule_event( $time, Settings::get( 'frequency' ), self::HOOK_NAME );
		}
	}

	/**
	 * Remove the scheduled job
	 */
	public function remove() {
		wp_clear_scheduled_hook( self::HOOK_NAME );
	}
}
